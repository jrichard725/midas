var LoginView = function (adapter, template, welcomeTemplate) {
    this.initialize = function () {
        // Define a div wrapper for the view. The div wrapper is used to attach events.
        this.el = $('<div/>');
    };

    this.initialize();

    this.render = function() {
        this.el.html(template());
        return this;
    };
}
