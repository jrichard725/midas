// We use an "Immediate Function" to initialize the application to avoid leaving anything behind in the global scope
(function () {
    /* ---------------------------------- Local Variables ---------------------------------- */

    // Setup constant variables
    var base_url = 'http://api.theinfiniteregistry.com/';// --> NEED TO SETUP valid ssl
    var key_prefix = 'inf_reg_';

	var googleapi = {
		authorize: function(options) {
			var deferred = $.Deferred();

			//Build the OAuth consent page URL
			var authUrl = 'https://accounts.google.com/o/oauth2/auth?' +
				$.param({
					client_id: options.client_id,
					redirect_uri: options.redirect_uri,
					response_type: 'code',
					scope: options.scope
				});

			//Open the OAuth consent page in the InAppBrowser
			var authWindow = window.open(authUrl
										 , '_blank'
										 , 'location=no,toolbar=no'
										);

			//has granted us access to their data.
			$(authWindow).on('loadstart', function(e) {
				var url = e.originalEvent.url;
				var code = /\?code=(.+)$/.exec(url);
				var error = /\?error=(.+)$/.exec(url);

				if (code || error) {
					//Always close the browser when match is found
					authWindow.close();
				}
				if (code) {
					console.log(code);
					//Exchange the authorization code for an access token
					$.post('https://accounts.google.com/o/oauth2/token', {
						code: code[1],
						client_id: options.client_id,
						client_secret: options.client_secret,
						redirect_uri: options.redirect_uri,
						grant_type: 'authorization_code'
					}).done(function(data) {
						deferred.resolve(data);
					}).fail(function(response) {
						deferred.reject(response.responseJSON);
					});
				} else if (error) {
					console.log(error);
					//The user denied access to the app
					deferred.reject({
						error: error[1]
					});
				}
			});

			return deferred.promise();
		}
	};

    // Setup Templates
    var loginTpl = Handlebars.compile($("#login-tpl").html());
    var welcomeTpl = Handlebars.compile($("#welcome-tpl").html());

    // Setup other objects
    var adapter = new LocalStorageAdapter();

    /* --------------------------------- Initialize the app -------------------------------- */
    adapter.initialize().done(function () {
        attempt_login();
    });

    /* --------------------------------- Event Registration -------------------------------- */
    document.addEventListener('deviceready', function () {
        if (navigator.notification) { // Override default HTML alert with native dialog
            window.alert = function (message) {
                navigator.notification.alert(
                    message,                 // message
                    null,                    // callback
                    "The Infinite Registry", // title
                    'OK'                     // buttonName
                );
            };
        }

        FastClick.attach(document.body);

    }, false);


    // Login
    $('#login-form').submit(function(e) {
        login($(this).serializeArray());
        return false;
    });

    /* ---------------------------------- Local Functions ---------------------------------- */
    function renderLoginScreen() {
		$('#view').html(loginTpl());
    }

    function renderWelcomeScreen(username) {
		$('#view').html(welcomeTpl({'username' : username}));
    }

    function login(values, stored) {
		// Set email and password variables for login
		if(stored){
			var email = values['email'];
			var password = values['password'];
		} else {
			var email = values[0]['value'];
			var password = values[1]['value'];
		}

        $.post(
            base_url+'password.php'
			, {  'email' : email
			   , 'password' : password }
            , function(response){
                if(response['status'] == 200){
					var username = response['data']['username'];
					if(!stored){
						//insert into local storage
						store_credentials(email, password, username);
					} else {
						renderWelcomeScreen(username);
					}
                } else {
					$('#login-error').slideUp().delay(400).slideDown();
				}
			}
			, 'json'
		);
    }

	function attempt_login(){
		var stored_email = window.localStorage.getItem(key_prefix+"email");
		var stored_pass = window.localStorage.getItem(key_prefix+"password");

		if (stored_email !== 'undefined' && stored_pass !== 'undefined'
		   && stored_email && stored_pass){
			login({'email':stored_email, 'password':stored_pass}, true);
		} else {
			renderLoginScreen()
		}
	}

	function store_credentials(email, pass, username){
		window.localStorage.setItem(key_prefix+"email", email);
		window.localStorage.setItem(key_prefix+"password", pass);
		renderWelcomeScreen(username);
	}

	//Function to handle google+ login
	$(document).on('deviceready', function() {
		var $loginButton = $('#google-login a');
		var $loginStatus = $('#google-login p');

		$loginButton.on('click', function() {
			googleapi.authorize({
				client_id: '87482271857-j7lc5gpd2hod74jrctq8c1ssta8904ni.apps.googleusercontent.com',
				redirect_uri: 'http://localhost',
				scope: 'https://www.googleapis.com/auth/plus.login'
			}).done(function(data) {
				$loginStatus.html('Access Token: ' + data.access_token);
			}).fail(function(data) {
				$loginStatus.html(data.error);
			});
		});
	});

}());